# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Java library for Soyal access control devices. It can use for make commands easy and send to the device or devices.
* 0.1 Beta
* [Repository](https://bitbucket.org/n3d4ti/soyal-lib)
* [WIKI](https://bitbucket.org/n3d4ti/soyal-lib/wiki/Home)

### How do I get set up? ###

1. Clone:
```
$ git clone https://n3d4ti@bitbucket.org/n3d4ti/soyal-lib.git
```

2. Build

3. Add to your classpath or add as maven dependency


* Configuration
* Dependencies
* How to run tests: Use maven test lifecycle
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Attila Nedbal - nedbal.attila@gmail.com
* Other community or team contact