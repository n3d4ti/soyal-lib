package com.nedlab.soyal.lib.utils;

import com.nedlab.soyal.lib.objects.Event;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Created by attila on 30/10/16.
 */
public class EventUtilTest {
    @Test
    public void parseEvent() throws Exception {
        System.out.println("EventUtilTest:parseEvent");

        byte[] respond = {0x7E, 0x1D, 0x00, (byte) 0x0B, (byte) 0x01,
                (byte) 0x36, 0x21, 0x02, 0x02, 0x05, 0x06, 0x06, 0x01, 0x00, 0x01, 0x00, 0x00, 0x18, 0x00,
                0x63, 0x6B, 0x01, 0x14, (byte)0xB8, (byte)0xB4, (byte)0xEE, (byte)0xCF};

        //Create expect event
        Event expectEvent = new Event();
        expectEvent.setEventtype("Normal Access");
        expectEvent.setSiteCode(25451);
        expectEvent.setCardCode(47284);
        expectEvent.setUserAddress(1);
        Calendar cal = Calendar.getInstance();
        cal.set(2006, 6, 5, 2, 33, 54);
        expectEvent.setTimeStamp(cal.getTime());
        expectEvent.setDirection("Enter");
        expectEvent.setDoorID(123);

        Event actualevent = EventUtil.ParseEvent(respond, 123);

        assertEquals(expectEvent.toString(), actualevent.toString());

    }

}