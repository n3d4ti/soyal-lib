package com.nedlab.soyal.lib.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.nedlab.soyal.lib.objects.CardContent;

/**
 * Created by attila on 01/11/16.
 */
public class CardUtilTest {
	@Test
	public void parseCardContent() throws Exception {
		System.out.println("CardUtilTest:parseCardContent");

		byte[] respond = { 0x7E, 0x0D, 0x00, 0x03, 0x01, 0x04, 0x41, (byte) 0xEA, 0x4B, 0x04, (byte) 0xD2, 0x02, 0x0B,
				(byte) 0xC6, 0x27 };

		// Create expected CarcContent
		CardContent expected = new CardContent();
		expected.setSitecode(1089);
		expected.setCardcode(59979);
		expected.setPin(1234);
		expected.setAccessmode(2);
		expected.setTimezone(11);

		CardContent actual = new CardContent(respond);

		assertEquals(expected.toString(), actual.toString());
	}

}