package com.nedlab.soyal.lib.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 06/09/16.
 */
public class ConvertersTest {
    @Test
    public void stringToByteArray() throws Exception {
        System.out.println("ConvertersTest:stringToByteArray");
        byte[] actual = Converters.StringToByteArray("7e0401857b01");
        byte[] expect = {0x7E, 0x04, 0x01, (byte) 0x85, (byte) 0x7B, (byte) 0x01};

        assertArrayEquals(expect, actual);
    }

}