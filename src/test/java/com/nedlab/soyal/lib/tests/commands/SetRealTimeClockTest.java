package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.SetRealTimeClock;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class SetRealTimeClockTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("SetRealTimeClockTest:buildCommand");
        SetRealTimeClock command = new SetRealTimeClock((byte)0x01, (byte)0x00, (byte)0x01, (byte)0x02, (byte)0x03, (byte)0x04, (byte)0x05, (byte)0x06);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x0B, 0x01, 0x23, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, (byte)0xDA, (byte)0x13};

        assertArrayEquals(expect, actual);
    }

}