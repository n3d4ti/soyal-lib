package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.GetRealTimeClock;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 19/08/16.
 */
public class GetRealTimeClockTest {

    @Test
    public void buildCommand() throws Exception {
        System.out.println("GetRealTimeClockTest:buildCommand");
        GetRealTimeClock command = new GetRealTimeClock((byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, 0x24, (byte) 0xDA, (byte)0xFF};

        assertArrayEquals(expect, actual);
    }

}