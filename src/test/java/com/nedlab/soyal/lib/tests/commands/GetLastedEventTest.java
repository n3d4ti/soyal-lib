package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.GetLastedEvent;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class GetLastedEventTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("GetLastedEventTest:buildCommand");
        GetLastedEvent command = new GetLastedEvent((byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, 0x25, (byte) 0xDB, (byte)0x01};

        assertArrayEquals(expect, actual);
    }

}