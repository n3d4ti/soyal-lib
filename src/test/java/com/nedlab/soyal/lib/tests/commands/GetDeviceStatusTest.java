package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.GetDeviceStatus;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class GetDeviceStatusTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("GetDeviceStatusTest:buildCommand");
        GetDeviceStatus command = new GetDeviceStatus((byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, 0x18, (byte) 0xE6, (byte)0xFF};

        assertArrayEquals(expect, actual);
    }

}