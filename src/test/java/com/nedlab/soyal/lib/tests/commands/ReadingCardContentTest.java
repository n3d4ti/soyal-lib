package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.ReadingCardContent;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 17/10/16.
 */
public class ReadingCardContentTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("ReadingCardContentTest:buildCommand");
        ReadingCardContent command = new ReadingCardContent((byte)0x01, 2, (byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x07, 0x01, (byte)0x87, (byte) 0x00, (byte)0x02, 0x01, 0x7A, 0x05};

        assertArrayEquals(expect, actual);
    }

}