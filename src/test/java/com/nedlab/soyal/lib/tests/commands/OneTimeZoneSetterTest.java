package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.OneTimeZoneSetter;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class OneTimeZoneSetterTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("OneTimeZoneSetterTest:buildCommand");
        OneTimeZoneSetter command = new OneTimeZoneSetter((byte)0x01);
        command.setIdx((byte) 0x01);
        command.setMon(8, 0, 15, 0);
        command.setTue(8, 0, 15, 0);
        command.setWed(8, 0, 15, 0);
        command.setThu(8, 0, 15, 0);
        command.setFri(8, 0, 15, 0);
        command.setSat(8, 0, 15, 0);
        command.setSun(8, 0, 15, 0);

        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x24, 0x01, 0x2A, (byte) 0x01, (byte)0x01,
        0x0C, 0x00, 0x01, (byte)0xE0, 0x03, (byte)0x84, 0x01, (byte)0xE0, 0x03, (byte)0x84,
                0x01, (byte)0xE0, 0x03, (byte)0x84, 0x01, (byte)0xE0, 0x03, (byte)0x84,
                0x01, (byte)0xE0, 0x03, (byte)0x84, 0x01, (byte)0xE0, 0x03, (byte)0x84,
                0x01, (byte)0xE0, 0x03, (byte)0x84, (byte)0xBE, (byte)0xCF };

        assertArrayEquals(expect, actual);
    }

}