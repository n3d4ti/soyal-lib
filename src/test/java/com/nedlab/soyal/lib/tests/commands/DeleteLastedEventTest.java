package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.DeleteLastedEvent;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class DeleteLastedEventTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("DeleteLastedEventTest:buildCommand");
        DeleteLastedEvent command = new DeleteLastedEvent((byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, 0x37, (byte) 0xC9, (byte)0x01};

        assertArrayEquals(expect, actual);
    }

}