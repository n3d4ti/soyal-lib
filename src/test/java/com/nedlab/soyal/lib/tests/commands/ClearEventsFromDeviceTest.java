package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.ClearEventsFromDevice;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class ClearEventsFromDeviceTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("ClearEventsFromDeviceTest:buildCommand");
        ClearEventsFromDevice command = new ClearEventsFromDevice((byte)0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, 0x2D, (byte) 0xD3, (byte)0x01};

        assertArrayEquals(expect, actual);
    }

}