package com.nedlab.soyal.lib.tests.commands;

import com.nedlab.soyal.lib.commands.common.ClearAllCardContent;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by attila on 20/08/16.
 */
public class ClearAllCardContentTest {
    @Test
    public void buildCommand() throws Exception {
        System.out.println("ClearAllCardContentTest:buildCommand");
        ClearAllCardContent command = new ClearAllCardContent((byte) 0x01);
        byte[] actual = command.getCommand();
        byte[] expect = {0x7E, 0x04, 0x01, (byte) 0x85, (byte) 0x7B, (byte) 0x01};

        assertArrayEquals(expect, actual);
    }

}