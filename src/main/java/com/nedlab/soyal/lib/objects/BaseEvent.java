package com.nedlab.soyal.lib.objects;

import com.nedlab.soyal.lib.utils.Converters;

/**
 * Created by attila on 26/09/16.
 */
public class BaseEvent {
	private byte head;
	private byte length;
	private byte node;
	private byte function;
	private byte readerId;

	protected byte[] data;
	private byte xor;
	private byte sum;

	public BaseEvent() {
	}

	public BaseEvent(final byte[] respond) {
		this.head = respond[0];
		this.length = respond[1];
		this.node = respond[2];
		this.function = respond[3];
		if (this.hasEvent()) {
			this.readerId = respond[4];
			this.data = this.getEventData(respond);
			this.xor = respond[respond.length - 2];
			this.sum = respond[respond.length - 1];
		}
	}

	public byte[] getData() {
		return this.data;
	}

	public int getDataByIndex(final int index) {
		return Converters.byteOverflow(this.data[index]);
	}

	private byte[] getEventData(final byte[] respond) {
		final byte[] result = new byte[20];
		for (int i = 0; i < result.length; i++) {
			result[i] = respond[i + 5];
		}
		return result;
	}

	public byte getFunction() {
		return this.function;
	}

	public byte getHead() {
		return this.head;
	}

	public byte getLength() {
		return this.length;
	}

	public byte getNode() {
		return this.node;
	}

	public byte getReaderId() {
		return this.readerId;
	}

	public byte getSum() {
		return this.sum;
	}

	public byte getXor() {
		return this.xor;
	}

	public boolean hasEvent() {
		boolean result = true;
		if (this.getFunction() != 0 && this.getFunction() == 0x04 && this.getLength() == 0x05) {
			result = false;
		}
		return result;
	}

	public void setData(final byte[] data) {
		this.data = data;
	}

	public void setFunction(final byte function) {
		this.function = function;
	}

	public void setHead(final byte head) {
		this.head = head;
	}

	public void setLength(final byte length) {
		this.length = length;
	}

	public void setNode(final byte node) {
		this.node = node;
	}

	public void setReaderId(final byte readerId) {
		this.readerId = readerId;
	}

	public void setSum(final byte sum) {
		this.sum = sum;
	}

	public void setXor(final byte xor) {
		this.xor = xor;
	}
}
