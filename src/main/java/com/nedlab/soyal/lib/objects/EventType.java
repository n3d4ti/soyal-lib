package com.nedlab.soyal.lib.objects;

/**
 * Created by attila on 30/10/16.
 */
public enum EventType {
    PIN_ERROR {
        public String toString() {
            return "PIN Error";
        }
    },

    INVALID_CARD {
        public String toString() {
            return "Invalid Card";
        }
    },

    TIME_ZONE_ERROR {
        public String toString() {
            return "Time Zone Error";
        }
    },

    NORMAL_ACCESS {
        public String toString() {
            return "Normal Access";
        }
    },

    EGRESS {
        public String toString() {
            return "Egress";
        }
    },

    ALARM_EVENT {
        public String toString() {
            return "Alarm Event";
        }
    },

    ANTIPASSBACK_ERROR{
        public String toString() {
            return "Antipassback Error";
        }
    }
}
