package com.nedlab.soyal.lib.objects;

import java.util.Calendar;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

/**
 * Created by attila on 19/08/16.
 */
public class Event extends BaseEvent {
	@SerializedName("EventType")
	private String eventType; // normal acces, antipassback, invalid card etc.

	@SerializedName("Direction")
	private String direction; // exit or entry

	@SerializedName("CardCode")
	private int cardCode;

	@SerializedName("SiteCode")
	private int siteCode;

	@SerializedName("UserAddress")
	private int userAddress;

	@SerializedName("TimeStamp")
	private Date timeStamp;

	@SerializedName("DoorID")
	private int doorID;

	public Event() {
	}

	public Event(final byte[] respond, final int doorID) {
		super(respond);
		if (this.hasEvent()) {
			this.initialize();
			this.doorID = doorID;
		}
	}

	public int getCardCode() {
		return this.cardCode;
	}

	public String getDirection() {
		return this.direction;
	}

	public int getDoorID() {
		return this.doorID;
	}

	public String getEventType() {
		return this.eventType;
	}

	public int getSiteCode() {
		return this.siteCode;
	}

	public Date getTimeStamp() {
		return this.timeStamp;
	}

	public String getUID() {

		return "";
	}

	public int getUserAddress() {
		return this.userAddress;
	}

	private void initialize() {
		this.setEventType();
		this.setDirection();
		this.setCardcode();
		this.setSitecode();
		this.setUserAddress();
		this.setTimeStamp();
	}

	private void setCardcode() {
		final int cardcode = this.getDataByIndex(18) * 256 + this.getDataByIndex(19);
		this.setCardCode(cardcode);
	}

	public void setCardCode(final int cardCode) {
		this.cardCode = cardCode;
	}

	private void setDirection() {
		final byte[] data = this.getData();
		if (data != null && data[13] == 0x80) {
			this.setDirection("Exit");
		} else {
			this.setDirection("Enter");
		}
	}

	public void setDirection(final String direction) {
		this.direction = direction;
	}

	public void setDoorID(final int doorID) {
		this.doorID = doorID;
	}

	private void setEventType() {
		switch (this.getFunction()) {
			case 0x03:
				this.setEventtype(EventType.INVALID_CARD.toString());
				break;
			case 0x04:
				this.setEventtype(EventType.TIME_ZONE_ERROR.toString());
				break;
			case 0x0B:
				this.setEventtype(EventType.NORMAL_ACCESS.toString());
				break;
			case 0x1E:
				this.setEventtype(EventType.ANTIPASSBACK_ERROR.toString());
				break;
			case 0x01:
				this.setEventtype(EventType.PIN_ERROR.toString());
				break;
			case 0x10:
				this.setEventtype(EventType.EGRESS.toString());
				break;
			case 0x11:
				this.setEventtype(EventType.ALARM_EVENT.toString());
				break;
		}
	}

	public void setEventtype(final String eventType) {
		this.eventType = eventType;
	}

	private void setSitecode() {
		final int sitecode = this.getDataByIndex(14) * 256 + this.getDataByIndex(15);
		this.setSiteCode(sitecode);
	}

	public void setSiteCode(final int siteCode) {
		this.siteCode = siteCode;
	}

	private void setTimeStamp() {
		final Calendar timestamp = Calendar.getInstance();
		timestamp.set(2000 + this.getDataByIndex(6), this.getDataByIndex(5), this.getDataByIndex(4),
				this.getDataByIndex(2), this.getDataByIndex(1), this.getDataByIndex(0));
		this.setTimeStamp(timestamp.getTime());
	}

	public void setTimeStamp(final Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	private void setUserAddress() {
		final int useraddress = this.getDataByIndex(8) * 256 + this.getDataByIndex(9);
		this.setUserAddress(useraddress);
	}

	public void setUserAddress(final int userAddress) {
		this.userAddress = userAddress;
	}

	@Override
	public String toString() {
		return this.getTimeStamp() + " " + this.getEventType() + " " + this.getDirection() + " useraddress: "
				+ this.getUserAddress() + " cardcode:sitecode" + this.getCardCode() + ":" + this.getSiteCode();
	}
}
