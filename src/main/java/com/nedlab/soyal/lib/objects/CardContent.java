package com.nedlab.soyal.lib.objects;

import com.nedlab.soyal.lib.utils.CheckSum;
import com.nedlab.soyal.lib.utils.Converters;

/**
 * Created by attila on 01/11/16.
 */
public class CardContent {
    private int sitecode;
    private int cardcode;
    private int pin;
    private int accessmode;
    private int timezone;


    public CardContent(){ }

    public CardContent(byte[] respond){
        if (isValidContent(respond)){
            setSiteCode(respond);
            setCardCode(respond);
            setPin(respond);
            setAccessMode(respond);
            setTimeZone(respond);
        }
    }

    private void setSiteCode(byte[] param){
        int sitecode = getDataByIndex(5, param) * 256 + getDataByIndex(6, param);
        setSitecode(sitecode);
    }

    private void setCardCode(byte[] param){
        int cardcode = getDataByIndex(7, param) * 256 + getDataByIndex(8, param);
        setCardcode(cardcode);
    }

    private void setPin(byte[] param){
        int pin = getDataByIndex(9, param) * 256 + getDataByIndex(10, param);
        setPin(pin);
    }

    private void setAccessMode(byte[] param){
        int am = getDataByIndex(11, param);
        setAccessmode(am);
    }

    private void setTimeZone(byte[] param){
        int tz = getDataByIndex(12, param);
        setTimezone(tz);
    }

    private boolean isValidContent(byte[] param){
        boolean result = false;
        byte xor = CheckSum.xor(getSection(2, param.length-3, param));
        byte sum = CheckSum.sum(getSection(2, param.length-2, param));
        if (xor == param[param.length-2] && sum == param[param.length-1])
            result = true;

        return result;
    }

    protected byte[] getSection(int start, int end, byte[] paramArray){
        int size = end - start + 1;
        byte[] result = new byte[size];
        int j = 0;
        for (int i = start; i <= end; i++) {
            result[j] = paramArray[i];
            j++;
        }
        return result;
    }

    public int getSitecode() {
        return sitecode;
    }

    public void setSitecode(int sitecode) {
        this.sitecode = sitecode;
    }

    public int getCardcode() {
        return cardcode;
    }

    public void setCardcode(int cardcode) {
        this.cardcode = cardcode;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getAccessmode() {
        return accessmode;
    }

    public void setAccessmode(int accessmode) {
        this.accessmode = accessmode;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    private int getDataByIndex(int index, byte[] param){
        return Converters.byteOverflow(param[index]);
    }

    @Override
    public String toString() {
        return "Card Content: " + getCardcode() + ":" + getSitecode() + "(cardcode:sitecode) pin: " + getPin() +
                " accessmode: " + getAccessmode() + " timezone: " + getTimezone();
    }
}
