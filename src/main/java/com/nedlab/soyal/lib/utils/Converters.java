package com.nedlab.soyal.lib.utils;

import java.math.BigInteger;

/**
 * Created by attila on 06/09/16.
 */

public class Converters {

	public static byte[] StringToByteArray(String param) {
		return new BigInteger(param, 16).toByteArray();
	}

	public static int byteOverflow(byte param) {
		int result = param;
		if (param < 0) {
			result = param + 256;
		}
		return result;
	}

	public static void ByteWriter(byte[] param) {
		for (int j = 0; j < param.length; j++) {
			System.out.format("%02X ", param[j]);
		}
		System.out.println();
	}

}
