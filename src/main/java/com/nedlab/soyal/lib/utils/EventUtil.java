package com.nedlab.soyal.lib.utils;

import com.nedlab.soyal.lib.objects.Event;

/**
 * Created by attila on 26/09/16.
 */
public class EventUtil {

	public static Event ParseEvent(final byte[] param, final int doorID) {
		final Event result = new Event(param, doorID);
		return result;
	}

}
