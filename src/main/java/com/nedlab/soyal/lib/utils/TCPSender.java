package com.nedlab.soyal.lib.utils;

import com.nedlab.soyal.lib.commands.common.BaseCommand;

import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by attila on 20/08/16.
 */
public class TCPSender {

    private BaseCommand command;
    private byte[] response;

    /**
     *
     * @param host destination host
     * @param port destination port
     * @param timeout - socket connection timeout, nullable
     *                null: it means default timeout
     *                0: it means maximum timeout (60000)
     */
    public void send(String host, int port, int timeout){
        Socket clientSocket = null;
        try {
            clientSocket = new Socket(host, port);

            if(timeout == 0) clientSocket.setSoTimeout(60000);

            final DataOutputStream outputStream = new DataOutputStream(clientSocket.getOutputStream());
            outputStream.write(command.getCommand());

            //Wait for response
            Thread.sleep(2000);
            this.response = receive(clientSocket.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected byte[] receive(final InputStream inputStream) {
        List<Integer> integer_list = null;
        byte[] result = null;
        if (inputStream != null) {
            integer_list = new ArrayList<Integer>();
            BufferedReader fromClient = null;
            try {
                fromClient = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));

                integer_list.add(fromClient.read());
                final int len = fromClient.read();
                integer_list.add(len);

                for (int i = 2; i < len; i++) {
                    integer_list.add(fromClient.read());
                }

                Iterator<Integer> iterator = integer_list.iterator();
                result = new byte[integer_list.size()];

                int index = 0;
                while(iterator.hasNext())
                {
                    Integer i = iterator.next();
                    result[index] = i.byteValue();
                    index++;
                }

            } catch (final SocketTimeoutException e) {
                System.out.println(e.getMessage());
            } catch (final IOException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (fromClient != null) {
                        fromClient.close();
                    }
                } catch (final IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return result;
    }

    public BaseCommand getCommand() {
        return command;
    }

    public void setCommand(BaseCommand command) {
        this.command = command;
    }

    public byte[] getResponse() {
        return response;
    }
}
