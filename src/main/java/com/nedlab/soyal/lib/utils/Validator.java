package com.nedlab.soyal.lib.utils;

/**
 * Created by attila on 20/08/16.
 */
public class Validator {

    /**
     * Check the CRC of the response byte array
     * @param response
     * @return
     */
    public static boolean isValidResponse(byte[] response){
        byte xor = 0;
        byte sum = 0;
        boolean result = false;

        if(response != null && response.length > 2) {
            xor = response[response.length - 2];
            sum = response[response.length - 1];
        }

        byte[] crc = doCheckSum(response);

        try {

            // Check the CRC section
            if(crc[0] == xor && crc[1] == sum)
                result = true;

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return result;
    }

    private static byte[] doCheckSum(byte[] response){
        byte[] result = new byte[2];

        result[0] = CheckSum.xor(getSection(2, response.length-3, response));
        result[1] = CheckSum.sum(getSection(2, response.length-2, response));

        return result;
    }

    private static byte[] getSection(int start, int end, byte[] paramArray){
        int size = end - start + 1;
        byte[] result = new byte[size];
        int j = 0;
        for (int i = start; i <= end; i++) {
            result[j] = paramArray[i];
            j++;
        }
        return result;
    }
}
