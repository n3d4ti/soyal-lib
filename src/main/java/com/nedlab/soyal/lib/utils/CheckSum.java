package com.nedlab.soyal.lib.utils;

/**
 * Created by attila on 19/08/16.
 */
public class CheckSum {
    public static byte xor(byte[] param){
        byte result = (byte) 0xFF;
        if (param != null && param.length > 0){
            for (byte h : param) {
                result ^= h;
            }
        }
        return result;
    }

    public static byte sum(byte[] param){
        byte result = 0;
        if (param != null && param.length > 0){
            for (byte h : param) {
                result += h;
            }
        }
        result %= 256;
        return result;
    }
}
