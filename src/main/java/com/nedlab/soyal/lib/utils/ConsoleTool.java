package com.nedlab.soyal.lib.utils;

import java.util.Scanner;

/**
 * Created by attila on 01/11/16.
 */
import com.nedlab.soyal.lib.commands.common.BaseCommand;
import com.nedlab.soyal.lib.commands.common.ClearAllCardContent;
import com.nedlab.soyal.lib.commands.common.ClearEventsFromDevice;
import com.nedlab.soyal.lib.commands.common.DeleteLastedEvent;
import com.nedlab.soyal.lib.commands.common.GetDeviceStatus;
import com.nedlab.soyal.lib.commands.common.GetLastedEvent;
import com.nedlab.soyal.lib.commands.common.GetRealTimeClock;
import com.nedlab.soyal.lib.commands.common.LinkedTimeZoneSetter;
import com.nedlab.soyal.lib.commands.common.OneTimeZoneSetter;
import com.nedlab.soyal.lib.objects.Event;

public class ConsoleTool {
	/**
	 * @param args
	 *            the command line arguments
	 */

	static String ip;
	static int port;
	static int timeout;
	static String serialname;
	static int initChoos;
	static int commandChoos;
	static byte[] respond;

	public static void main(String[] args) {

		showInitMenu();

		showCommandMenu();

		while (true) {
			@SuppressWarnings("resource")
			Scanner in = new Scanner(System.in);
			commandChoos = in.nextInt();

			if (commandChoos == 0)
				System.exit(0);

			switch (commandChoos) {
			case 1:
				clearAllCardContent();
				showCommandMenu();
				break;
			case 2:
				clearAllEventsFromDevice();
				showCommandMenu();
				break;
			case 3:
				deleteLastedEvent();
				showCommandMenu();
				break;
			case 4:
				getDeviceStatus();
				showCommandMenu();
				break;
			case 5:
				getLastedEvent();
				showCommandMenu();
				break;
			// ...
			case 7:
				SendLinkedTimeZoneSetter();
				showCommandMenu();
				break;
			case 8:
				SendOneTimeZoneSetter();
				showCommandMenu();
				break;
			}
		}

	}

	private static void showInitMenu() {
		/**
		 * Init MENU
		 */
		System.out.println(" ########################## ");
		System.out.println(" # Soyal CommandLine Tool # ");
		System.out.println(" ########################## ");
		System.out.println(" Kommunikációs csatorna választása ");
		System.out.println(" ---------------------- ");
		System.out.println("[21] : TCP/IP");
		System.out.println("[22] : Serial");
		System.out.println(" ---------------------- ");

		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		initChoos = in.nextInt();

		if (initChoos == 21)
			setlanconfig();

		if (initChoos == 22)
			setserialconfig();

	}

	private static void setserialconfig() {

	}

	private static void setlanconfig() {
		System.out.println("Kérem adja meg a host ip címet!");
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		ip = in.nextLine();

		System.out.println("Kérem adja meg a host portot!");
		port = in.nextInt();

		System.out.println("Kérem adja meg a maximum várakozási időt (timeout)");
		timeout = in.nextInt();

		System.out.println(" ---------------------- ");
		System.out.println(" Megadott adatok: ");
		System.out.println(" ---------------------- ");
		System.out.println("IP/HOST: " + ip);
		System.out.println("PORT: " + port);
		System.out.println("Timeout: " + timeout);
		System.out.println(" ---------------------- ");

	}

	private static void showCommandMenu() {
		/**
		 * Command MENU
		 */
		System.out.println(" ########################## ");
		System.out.println(" # Soyal CommandLine Tool #");
		System.out.println(" ########################## ");
		System.out.println("[1] : Clear All Card Content");
		System.out.println("[2] : Clear Events from device");
		System.out.println("[3] : Delete Lasted Event");
		System.out.println("[4] : Get Device Status");
		System.out.println("[5] : Get Lasted Event");
		System.out.println("[6] : Get Real Time Clock");
		System.out.println("[7] : Linked TimeZone Setter");
		System.out.println("[8] : Simple Time Zone Setter");
		System.out.println("[9] : Reading Card Content");
		System.out.println("[10] : Relay Control");
		System.out.println("[11] : Set Real Time Clock");

		System.out.println("[0] : Exit");
	}

	public static void sendCommandTCP(BaseCommand command) {
		TCPSender sender = new TCPSender();
		sender.setCommand(command);
		sender.send(ip, port, timeout);
		System.out.println("Parancs küldve!");
		System.out.println("Nyers hexa válasz:");
		respond = sender.getResponse();
		Converters.ByteWriter(respond);
		System.out.println("----------");

	}

	public void getRealTimeClock() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		GetRealTimeClock grtc = new GetRealTimeClock(node);
		Converters.ByteWriter(grtc.getCommand());
		sendCommandTCP(grtc);
	}

	public static void getLastedEvent() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		GetLastedEvent gle = new GetLastedEvent(node);
		Converters.ByteWriter(gle.getCommand());
		sendCommandTCP(gle);
		Event ev = new Event(respond, 0);
		System.out.println("Esemény adatai:");
		ev.toString();
		System.out.println("----------");
	}

	public static void getDeviceStatus() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		GetDeviceStatus gds = new GetDeviceStatus(node);
		Converters.ByteWriter(gds.getCommand());
		sendCommandTCP(gds);
	}

	public static void clearAllCardContent() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		ClearAllCardContent clecc = new ClearAllCardContent(node);

		Converters.ByteWriter(clecc.getCommand());
		sendCommandTCP(clecc);
	}

	public static void clearAllEventsFromDevice() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		ClearEventsFromDevice cle = new ClearEventsFromDevice(node);

		Converters.ByteWriter(cle.getCommand());
		sendCommandTCP(cle);
	}

	public static void deleteLastedEvent() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		DeleteLastedEvent dle = new DeleteLastedEvent(node);

		Converters.ByteWriter(dle.getCommand());
		sendCommandTCP(dle);
	}

	private static void SendOneTimeZoneSetter() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Olvasó (node) azonosító?");
		byte node = 1;
		node = in.nextByte();

		OneTimeZoneSetter tz = new OneTimeZoneSetter(node);

		System.out.println("Hanyas sorszámú időzóna?");
		tz.setIdx(in.nextByte());
		int starth = 0;
		int startm = 0;
		int endh = 0;
		int endm = 0;

		System.out.println("----------");
		System.out.println("Hétfő");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setMon(starth, startm, endh, endm);
		System.out.println("----- Hétfő beállítva -------");

		System.out.println("----------");
		System.out.println("Kedd");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setTue(starth, startm, endh, endm);
		System.out.println("----- Kedd beállítva -------");

		System.out.println("----------");
		System.out.println("Szerda");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setWed(starth, startm, endh, endm);
		System.out.println("----- Szerda beállítva -------");

		System.out.println("----------");
		System.out.println("Csütörtök");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setThu(starth, startm, endh, endm);
		System.out.println("----- Csütörtök beállítva -------");

		System.out.println("----------");
		System.out.println("Péntek");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setFri(starth, startm, endh, endm);
		System.out.println("----- Péntek beállítva -------");

		System.out.println("----------");
		System.out.println("Szombat");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setSat(starth, startm, endh, endm);
		System.out.println("----- Szombat beállítva -------");

		System.out.println("----------");
		System.out.println("Vasárnap");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setSun(starth, startm, endh, endm);
		System.out.println("----- Vasárnap beállítva -------");

		Converters.ByteWriter(tz.getCommand());
		sendCommandTCP(tz);

	}

	private static void SendLinkedTimeZoneSetter() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		byte node = 1;
		byte next = 0;
		System.out.println("Olvasó (node) azonosító?");
		node = in.nextByte();

		System.out.println("Kapcsolt időzóna index");
		next = in.nextByte();

		LinkedTimeZoneSetter tz = new LinkedTimeZoneSetter(node, next);
		System.out.println("Hanyas sorszámú időzóna?");
		tz.setIdx(in.nextByte());

		int starth = 0;
		int startm = 0;
		int endh = 0;
		int endm = 0;

		System.out.println("----------");
		System.out.println("Hétfő");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setMon(starth, startm, endh, endm);
		System.out.println("----- Hétfő beállítva -------");

		System.out.println("----------");
		System.out.println("Kedd");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setTue(starth, startm, endh, endm);
		System.out.println("----- Kedd beállítva -------");

		System.out.println("----------");
		System.out.println("Szerda");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setWed(starth, startm, endh, endm);
		System.out.println("----- Szerda beállítva -------");

		System.out.println("----------");
		System.out.println("Csütörtök");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setThu(starth, startm, endh, endm);
		System.out.println("----- Csütörtök beállítva -------");

		System.out.println("----------");
		System.out.println("Péntek");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setFri(starth, startm, endh, endm);
		System.out.println("----- Péntek beállítva -------");

		System.out.println("----------");
		System.out.println("Szombat");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setSat(starth, startm, endh, endm);
		System.out.println("----- Szombat beállítva -------");

		System.out.println("----------");
		System.out.println("Vasárnap");
		System.out.println("----------");
		System.out.println("kezdeti óra:");
		starth = in.nextInt();
		System.out.println("kezdeti perc:");
		startm = in.nextInt();
		System.out.println("vége óra:");
		endh = in.nextInt();
		System.out.println("vége perc:");
		endm = in.nextInt();
		tz.setSun(starth, startm, endh, endm);
		System.out.println("----- Vasárnap beállítva -------");

		Converters.ByteWriter(tz.getCommand());
		sendCommandTCP(tz);

	}

}
