package com.nedlab.soyal.lib.utils;

import java.io.IOException;

import com.nedlab.soyal.lib.commands.common.BaseCommand;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 * Created by attila on 20/08/16.
 */

public class SerialSender {

	private BaseCommand command;
	private byte[] response;

	/**
	 *
	 * default 9600,N,8,1 - if baudrate, databits, stopbits and parity is 0
	 * 
	 * @param portname
	 * @param b
	 *            - baudrate
	 * @param d
	 *            - databits
	 * @param s
	 *            - stopbits
	 * @param p
	 *            - parity
	 */
	public void send(String portname, int b, int d, int s, int p) {
		// getting serial ports list into the array
		String[] portNames = SerialPortList.getPortNames();
		// check is there any ports
		if (portNames.length == 0) {
			System.out.println(
					"There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		SerialPort serialPort = new SerialPort(portname);

		try {
			serialPort.openPort();
			if (b == 0 && d == 0 && s == 0 && p == 0)
				serialPort.setParams(9600, 8, 1, 0);
			else
				serialPort.setParams(b, d, s, p);

			// wait for response
			Thread.sleep(2000);

			// after waiting 2 second read from port
			this.response = serialPort.readBytes();

		} catch (SerialPortException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public BaseCommand getCommand() {
		return command;
	}

	public void setCommand(BaseCommand command) {
		this.command = command;
	}

	public byte[] getResponse() {
		return response;
	}
}
