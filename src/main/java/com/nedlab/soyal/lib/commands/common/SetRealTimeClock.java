package com.nedlab.soyal.lib.commands.common;

/**
 * Created by attila on 19/08/16.
 */
public class SetRealTimeClock extends BaseCommand {
    byte node;
    private byte second;
    private byte minute;
    private byte hour;
    private byte dayofweek;
    private byte date;
    private byte month;
    private byte year;
    private static final byte commandcode = 0x23;

    public SetRealTimeClock(byte node, byte second, byte minute, byte hour, byte dayofweek, byte date, byte month, byte year) {
        // length from the documentation
        super((byte)0x0B);
        this.node = node;
        this.second = second;
        this.minute = minute;
        this.hour = hour;
        this.dayofweek = dayofweek;
        this.date = date;
        this.month = month;
        this.year = year;

        buildCommand();

        checksum();
    }

    public SetRealTimeClock(byte node) {
        // length from the documentation
        super((byte)0x0B);
        this.node = node;

        buildCommand();

        checksum();
    }

    @Override
    protected void buildCommand() {
        if (command != null){
            command[2] = node;
            command[3] = commandcode;
            command[4] = second;
            command[5] = minute;
            command[6] = hour;
            command[7] = dayofweek;
            command[8] = date;
            command[9] = month;
            command[10] = year;

        }
    }

    public byte getSecond() {
        return second;
    }

    public void setSecond(byte second) {
        this.second = second;
    }

    public byte getMinute() {
        return minute;
    }

    public void setMinute(byte minute) {
        this.minute = minute;
    }

    public byte getHour() {
        return hour;
    }

    public void setHour(byte hour) {
        this.hour = hour;
    }

    public byte getDayofweek() {
        return dayofweek;
    }

    public void setDayofweek(byte dayofweek) {
        this.dayofweek = dayofweek;
    }

    public byte getDate() {
        return date;
    }

    public void setDate(byte date) {
        this.date = date;
    }

    public byte getMonth() {
        return month;
    }

    public void setMonth(byte month) {
        this.month = month;
    }

    public byte getYear() {
        return year;
    }

    public void setYear(byte year) {
        this.year = year;
    }
}
