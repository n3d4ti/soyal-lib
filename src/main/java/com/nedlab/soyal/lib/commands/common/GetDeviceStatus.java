package com.nedlab.soyal.lib.commands.common;

/**
 * Created by attila on 20/08/16.
 */
public class GetDeviceStatus extends BaseCommand {
    byte node;
    private static final byte commandcode = 0x18;

    public GetDeviceStatus(byte node) {
        // length from the documentation
        super((byte)0x04);
        this.node = node;

        buildCommand();

        checksum();
    }

    @Override
    protected void buildCommand() {
        if (command != null){
            command[2] = node;
            command[3] = commandcode;
        }
    }
}
