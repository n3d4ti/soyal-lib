package com.nedlab.soyal.lib.commands.common;

/**
 * Created by Kornel on 2016.10.15..
 */
public class ReadingCardContent extends BaseCommand {
	byte node;
	private static final byte commandcode = (byte) 0x87;
	private byte addrH;
	private byte addrL;
	private byte numberOfCards;
	@SuppressWarnings("unused")
	private int userAddress;

	public ReadingCardContent(byte node, int userAddress, byte numberOfCards) {
		// length from the documentation
		super((byte) 0x07);
		this.node = node;
		this.userAddress = userAddress;
		this.numberOfCards = numberOfCards;
		addrL = (byte) (userAddress % 256);
		addrH = (byte) (userAddress / 256);
		buildCommand();
		checksum();
	}

	@Override
	protected void buildCommand() {
		if (command != null) {
			command[2] = node;
			command[3] = commandcode;
			command[4] = addrH;
			command[5] = addrL;
			command[6] = numberOfCards;
		}
	}

}