package com.nedlab.soyal.lib.commands.common;

/**
 * Created by attila on 21/08/16.
 */
public class LinkedTimeZoneSetter extends OneTimeZoneSetter {

    /**
     * If you set one or more timezones. You can link two or more timezones.
     * With this class can set one timezone and link it to another already set timezone.
     * @param node
     * @param next
     */
    public LinkedTimeZoneSetter(byte node, byte next) {
        super(node);
        setLinkIdx(next);
    }


}
