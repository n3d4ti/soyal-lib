package com.nedlab.soyal.lib.commands.common;

/**
 * Created by attila on 20/08/16.
 */
public class OneTimeZoneSetter extends BaseCommand {
    byte node;

    private static final byte commandcode = 0x2A;

    private byte[] sun;
    private byte[] mon;
    private byte[] tue;
    private byte[] wed;
    private byte[] thu;
    private byte[] fri;
    private byte[] sat;

    private byte idx;

    private Byte linkIdx;

    public OneTimeZoneSetter(byte node) {
        // length from the documentation
        super((byte)0x24);

        this.node = node;

        init();

    }

    @Override
    public byte[] getCommand() {
        buildCommand();
        checksum();
        return command;
    }

    @Override
    protected void buildCommand() {
        if (command != null){
            command[2] = node;
            command[3] = commandcode;
            command[4] = idx;
            command[5] = 0x01; // one timezone setting in this command
            command[6] = getLinkIdx(); // link to next - defatul value
            command[7] = 0x00; // timeZone level

            command[8] =  sun[0];
            command[9] =  sun[1];
            command[10] =  sun[2];
            command[11] =  sun[3];

            command[12] = mon[0];
            command[13] = mon[1];
            command[14] = mon[2];
            command[15] = mon[3];

            command[16] = tue[0];
            command[17] = tue[1];
            command[18] = tue[2];
            command[19] = tue[3];

            command[20] = wed[0];
            command[21] = wed[1];
            command[22] = wed[2];
            command[23] = wed[3];

            command[24] = thu[0];
            command[25] = thu[1];
            command[26] = thu[2];
            command[27] = thu[3];

            command[28] = fri[0];
            command[29] = fri[1];
            command[30] = fri[2];
            command[31] = fri[3];

            command[32] = sat[0];
            command[33] = sat[1];
            command[34] = sat[2];
            command[35] = sat[3];
        }
    }


    private void init(){
        this.mon = new byte[4];
        this.tue = new byte[4];
        this.wed = new byte[4];
        this.thu = new byte[4];
        this.fri = new byte[4];
        this.sat = new byte[4];
        this.sun = new byte[4];
    }

    public byte[] getSun() {
        return sun;
    }

    public void setSun(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.sun == null)
            this.sun = new byte[4];

        this.sun[0] = (byte) (beginINminutes / 256);
        this.sun[1] = (byte) (beginINminutes % 256);
        this.sun[2] = (byte) (endINminutes / 256);
        this.sun[3] = (byte) (endINminutes % 256);
    }

    public byte[] getMon() {
        return mon;
    }

    public void setMon(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.mon == null)
            this.mon = new byte[4];

        this.mon[0] = (byte) (beginINminutes / 256);
        this.mon[1] = (byte) (beginINminutes % 256);
        this.mon[2] = (byte) (endINminutes / 256);
        this.mon[3] = (byte) (endINminutes % 256);
    }

    public byte[] getTue() {
        return tue;
    }

    public void setTue(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.tue == null)
            this.tue = new byte[4];

        this.tue[0] = (byte) (beginINminutes / 256);
        this.tue[1] = (byte) (beginINminutes % 256);
        this.tue[2] = (byte) (endINminutes / 256);
        this.tue[3] = (byte) (endINminutes % 256);
    }

    public byte[] getWed() {
        return wed;
    }

    public void setWed(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.wed == null)
            this.wed = new byte[4];

        this.wed[0] = (byte) (beginINminutes / 256);
        this.wed[1] = (byte) (beginINminutes % 256);
        this.wed[2] = (byte) (endINminutes / 256);
        this.wed[3] = (byte) (endINminutes % 256);
    }

    public byte[] getThu() {
        return thu;
    }

    public void setThu(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.thu == null)
            this.thu = new byte[4];

        this.thu[0] = (byte) (beginINminutes / 256);
        this.thu[1] = (byte) (beginINminutes % 256);
        this.thu[2] = (byte) (endINminutes / 256);
        this.thu[3] = (byte) (endINminutes % 256);
    }

    public byte[] getFri() {
        return fri;
    }

    public void setFri(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.fri == null)
            this.fri = new byte[4];

        this.fri[0] = (byte) (beginINminutes / 256);
        this.fri[1] = (byte) (beginINminutes % 256);
        this.fri[2] = (byte) (endINminutes / 256);
        this.fri[3] = (byte) (endINminutes % 256);
    }

    public byte[] getSat() {
        return sat;
    }

    public void setSat(int beginhour, int beginmin, int endhour, int endmin) {
        int beginINminutes = beginhour * 60 + beginmin;
        int endINminutes = endhour * 60 + endmin;

        if(this.sat == null)
            this.sat = new byte[4];

        this.sat[0] = (byte) (beginINminutes / 256);
        this.sat[1] = (byte) (beginINminutes % 256);
        this.sat[2] = (byte) (endINminutes / 256);
        this.sat[3] = (byte) (endINminutes % 256);
    }

    public byte getIdx() {
        return idx;
    }

    public void setIdx(byte idx) {
        this.idx = idx;
    }


    protected byte getLinkIdx() {
        return linkIdx == null ? 0x0C : linkIdx;
    }

    protected void setLinkIdx(byte linkIdx) {
        this.linkIdx = linkIdx;
    }

}
