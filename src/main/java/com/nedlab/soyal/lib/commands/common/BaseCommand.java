package com.nedlab.soyal.lib.commands.common;

import com.nedlab.soyal.lib.utils.CheckSum;

/**
 * Created by attila on 06/05/16.
 *
 * All of commands are started with header. 7E and define length All of commands
 * has checksum at the end of command
 *
 */

public abstract class BaseCommand {
	protected byte[] command;

	protected BaseCommand(byte length) {
		// Data Length Indicator which denotes the length
		// from Destination to the end including XOR and
		// SUM (must less then 250 bytes)
		// Adding to more bytes for the header and the length value.
		command = new byte[length + 2];

		// Set header
		if (command != null) {
			command[0] = 0x7E;
			command[1] = length;
		}
	}

	/**
	 * Must be override in child classes. Implement the build of the command.
	 */
	protected abstract void buildCommand();

	/**
	 * Checksum XOR byte
	 * 
	 * @return byte of xor from destination node byte to end of data.
	 */
	@SuppressWarnings("unused")
	private byte xor() {
		return CheckSum.xor(command);
	}

	/**
	 * Checksum SUM byte
	 * 
	 * @return byte of xor from destination node byte to XOR byte. SUM value at
	 *         the end of the byte array
	 */
	@SuppressWarnings("unused")
	private byte sum() {
		return CheckSum.sum(command);
	}

	/**
	 * Create the checksum for the command
	 */
	protected void checksum() {
		command[command.length - 2] = CheckSum.xor(getSection(2, command.length - 3, command));
		command[command.length - 1] = CheckSum.sum(getSection(2, command.length - 2, command));
	}

	/**
	 * Get the full command array which cn be send to RS485 or TCP port.
	 * 
	 * @return byte array with command values.
	 */
	public byte[] getCommand() {
		return command;
	}

	protected byte[] getSection(int start, int end, byte[] paramArray) {
		int size = end - start + 1;
		byte[] result = new byte[size];
		int j = 0;
		for (int i = start; i <= end; i++) {
			result[j] = paramArray[i];
			j++;
		}
		return result;
	}

}